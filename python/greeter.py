#!/usr/bin/python3
# -*- coding: utf-8 -*-
import proto.person_pb2


def greet_by_name(name):
	return f'Hello, {name}'


def person(name):
	return proto.person_pb2.Person(name = name)


def greet(person):
	return greet_by_name(person.name)
