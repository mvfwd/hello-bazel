
import proto.hello.PersonProto;

public class Greeter {

    public static String greet(String name) {
        return "Hello, " + name;
    }

    public static PersonProto.Person person(String name) {
        PersonProto.Person.Builder _person = PersonProto.Person.newBuilder();
        _person.setName(name);
        return _person.build();
    }

    public static String greet(PersonProto.Person _person) {
        return "Hello, " + _person.getName(); 
    }

}
