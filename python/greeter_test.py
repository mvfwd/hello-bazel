#!/usr/bin/python3
# -*- coding: utf-8 -*-
import greeter
import unittest


class GreeterTests(unittest.TestCase):
    
    def test_greet_by_name(self):
        self.assertEqual(greeter.greet_by_name('Alice'), 'Hello, Alice')

    def test_person(self):
        self.assertEqual(greeter.person('Alice').name, 'Alice')

    def test_greet(self):
        self.assertEqual(greeter.greet(greeter.person('Alice')), 'Hello, Alice')


if __name__ == '__main__':
    unittest.main()
