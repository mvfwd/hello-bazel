# hello-bazel

## Languages

- [x] C++
- [x] Python
- [x] Go
- [x] Java

## Notes

### Git

```bash
# Check for remote changes
$ git fetch

# Check for remote changes AND bring a copy
$ git pull

# Check available branches
$ git branch

# Create branch
$ git branch dev-bugfix

# Switch to branch
$ git checkout dev-bugfix

# Add, commit, push
$ git add .
$ git commit -m 'fix bug'
$ git push

# Merge multiple commits as one squashed commit
$ git checkout main
$ git merge --squash bugfix
$ git commit

# Delete branch remotely
$ git push origin -d dev-bugfix
# or
$ git push origin --delete dev-bugfix

# Delete branch locally
$ git branch -d dev-bugfix
# or
$ git branch --delete dev-bugfix
```
