
# Hello Bazel + Java

## Links

- [How To Install Java with Apt on Ubuntu 18.04](https://www.digitalocean.com/community/tutorials/how-to-install-java-with-apt-on-ubuntu-18-04)
- [Protocol Buffers in Bazel](https://blog.bazel.build/2017/02/27/protocol-buffers.html)
- [Bazel Tutorial: Build a Java Project](https://docs.bazel.build/versions/main/tutorial/java.html#bazel-tutorial-build-a-java-project)


## Setup

- Check Java version
	```bash
	$ java --version
	openjdk 14.0.2 2020-07-14
	OpenJDK Runtime Environment (build 14.0.2+12-Ubuntu-120.04)
	OpenJDK 64-Bit Server VM (build 14.0.2+12-Ubuntu-120.04, mixed mode, sharing)
	```
- Verify JDK installed
	```bash
	$ javac --version
	javac 11.0.11
	```
- Set JAVA_HOME
	```bash
	$ export JAVA_HOME="$(dirname $(dirname $(realpath $(which javac))))"
	```
- Check JAVA_HOME
	```bash
	$ echo $JAVA_HOME
	/usr/lib/jvm/java-11-openjdk-amd64
	```
- Build Java proto

## Build and Run

```bash
$ bazel build //java/com/example:all

$ ./bazel-bin/java/com/example/hello
Error: Could not find or load main class com.example.hello
Caused by: java.lang.ClassNotFoundException: com.example.hello
```

## Issues

### Cannot load repositories.bzl

https://github.com/bazelbuild/rules_java/issues/43

```bash
$ bazel build //proto:person_java_proto
ERROR: error loading package '': cannot load '@rules_java//java:repositories.bzl': no such file
INFO: Elapsed time: 0.101s
INFO: 0 processes.
FAILED: Build did NOT complete successfully (0 packages loaded)
```
