#include "greeter.hh"

namespace hello {

Person person(const std::string& name) {
	Person person;
	person.set_name(name);
	return person;
}

std::string greet(const std::string& name) {
	return "Hello, " + name;
}

std::string greet(const Person& person) {
	return "Hello, " + person.name();
}

}
