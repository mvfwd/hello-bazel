#include <gtest/gtest.h>
#include "greeter.hh"


namespace hello {

TEST(TestGreeter, greet) {
    ASSERT_EQ(greet("Alice"), "Hello, Alice");
}

TEST(TestGreeter, person) {
    ASSERT_EQ(person("Alice").name(), "Alice");
}

TEST(TestGreeter, greet_person) {
    ASSERT_EQ(greet(person("Alice")), "Hello, Alice");
}

}
