package hello

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGreetByName(t *testing.T) {
	assert.Equal(t, "Hello, Alice", GreetByName("Alice"))
}

func TestPerson(t *testing.T) {
	assert.Equal(t, "Alice", Person("Alice").GetName())
}

func TestGreet(t *testing.T) {
	assert.Equal(t, "Hello, Alice", Greet(Person("Alice")))
}
