#ifndef GREETER_HH
#define GREETER_HH

#include <string>
#include "proto/person.pb.h"

namespace hello {

Person person(const std::string& name);

std::string greet(const std::string& name);
std::string greet(const Person& person);

}

#endif
