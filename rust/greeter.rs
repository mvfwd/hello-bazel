use person_rust_proto::*;

pub fn greet(name: &str) -> String {
    format!("Hello, {}", name)
}

pub fn greet_person(person: &Person) -> String {
    format!("Hello, {}", person.get_name())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_greet() {
        assert_eq!(greet("Alice"), String::from("Hello, Alice"));
    }

    #[test]
    fn test_greet_person() {
        let mut person = Person::new();
        person.set_name(String::from("Bob"));
        assert_eq!(greet_person(&person), String::from("Hello, Bob"));
    }
}
