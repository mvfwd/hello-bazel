#!/usr/bin/python3
# -*- coding: utf-8 -*-
import greeter


def main():
	print(greeter.greet(greeter.person('Alice Python')))


if __name__ == '__main__':
	main()
