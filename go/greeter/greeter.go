package hello

import (
	pb "gazelle-prefix/person_go_proto"
)

func GreetByName(name string) string {
	return "Hello, " + name
}

func Person(name string) *pb.Person {
	return &pb.Person{
		Name: name,
	}
}

func Greet(person *pb.Person) string {
	return GreetByName(person.GetName())
}
